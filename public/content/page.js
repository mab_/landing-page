let content = `
# 📝 Les GitLab Pages avec GitLab CI

## Date du workshop: Mercredi 16 juin de 12h30 à 13h30

> **Avertissement**: ce workshop est complètement expérimental

## Détails / Contenu

Vous allez apprendre à publier des pages statiques simplement et facilement avec les **GitLab Pages** et **GitLab CI/CD**

## Prérequis

- Un navigateur (récent)
- Un compte sur **[GitLab.com](https://gitlab.com/)**
- Un compte sur **[Twitch](https://www.twitch.tv)**

## Comment s'inscrire à ce workshop

👋 **Vous devez vous inscrire avant le Mardi 15 juin de 12h00**

🖐️ **Attention**: seuls les 30 premiers inscrits seront enregistrés pour cette session.

- Allez dans les issues du projet **[registrations](https://gitlab.com/tanooki-workshops/20210616-gitlab-pages/registrations/-/issues)**
- Créer une issue dans le projet (avec juste un titre pour dire bonjour), et vous êtes inscrit (si vous êtes dans les 30 et avant la date limite)
- La veille (au plus tard) du workshop vous aurez accès au projet **[communications](https://gitlab.com/tanooki-workshops/20210616-gitlab-pages/communications)** avec tous les détails pour vous connecter dans cette issue 👋 [https://gitlab.com/tanooki-workshops/20210616-gitlab-pages/communications/-/issues/1](https://gitlab.com/tanooki-workshops/20210616-gitlab-pages/communications/-/issues/1)

à Mercredi 😃
`

let page =  {
  content: content
}

export {page}
