import {Style, Template, Router, Ossicle, Fragment} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'
import './MainTitle.js'
import './MarkDown.js'

class MainApplication extends Ossicle {

  static template() { return Template.html`
    <main-title></main-title>

    <div class="container">
      <article class="media">
        <div class="media-content">
          <mark-down file="../content/page.md"></mark-down>
        </div>
      </article>
    </div>
  `}

  static sheets() { return [
    Style.sheets.bulma
  ]}

  constructor() {
    super()
    this.setup()
  }

  connectedCallback() {

  }


}

customElements.define('main-application', MainApplication)

