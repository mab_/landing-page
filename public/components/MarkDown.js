import {Style, Template, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'

import {page} from '../content/page.js'

class MarkDown extends Ossicle {

  static sheets() { return [
    Style.sheets.highlight,
    Style.sheets.purple,
    Style.css`
      code {
        font-family: Menlo;
        border-radius: 8px;
      }
      p {
        font-size:1.1em;
      }
    `
  ]}

  static template() { return Template.html`
    <div class="container">
      <content>
        <h1>⏳ loading...</h1>
      </content>
    </div>
  `}


  constructor() {
    super()
    this.setup()
  }

  initialize() {
    this.$("content").innerHTML = window.markdownit().render(page.content)
    this.$all('code').forEach(code => {
      hljs.highlightBlock(code)
    })
  }

  connectedCallback() {
    this.initialize()
  }

}

customElements.define('mark-down', MarkDown)

